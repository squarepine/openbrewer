#ifndef SQUAREPINE_OPENBREWER_MODULE_DATA_H
#define SQUAREPINE_OPENBREWER_MODULE_DATA_H

#include "JuceHeader.h"

#include <vector>

//NB: The order of inclusions here matters.
#include "Units.h"
#include "Countries.h"

#include "Ingredient.h"
#include "Carbonation.h"
#include "Grain.h"
#include "Hops.h"
#include "Mash.h"
#include "Yeast.h"
#include "WaterProfile.h"

#include "Tool.h"
#include "Equipment.h"

#include "Style.h"
#include "AgingProfile.h"
#include "Recipe.h"

#endif //SQUAREPINE_OPENBREWER_MODULE_DATA_H
