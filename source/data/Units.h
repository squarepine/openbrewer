#ifndef SQUAREPINE_OPENBREWER_UNITS_H
#define SQUAREPINE_OPENBREWER_UNITS_H

/*
    Units:
        - Weight Units: kg, lbs, oz, g, mg, pkg, 100g
        - Hop units: kg, lbs, oz, g, mg, pkg, 100g
        - Batch Volume Units: Cup, pint, quart, gal, 12 oz, Barrel,
                              tsp, tbsp, 22 oz, fl oz, 375 mL, 750 mL,
                              imp oz, imp, pt, imp qt, imp gal, imp Barrel
        - Yeast starter volume: ^
        - Yeast units: ^
        - Mash volume units: ^
        - Gravity: SG, Brix, Plato
        - Refractometer: SG, Brix, RI
        - Colour units: EBC, SRM
        - Temperature: Celsius, Kelvin, Fahrenheit
        - Pressure: kPa, bar, atm, PSI
        - Elevation: feet, metres
    
    Bitterness formula: Tinseth, Rager, Garetz
    - First Wort Hop Adjustment: default; 10.00%
    - Mash Hop Adjustment: default; -80.00%
*/

#endif //SQUAREPINE_OPENBREWER_UNITS_H
