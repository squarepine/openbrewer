#ifndef SQUAREPINE_OPENBREWER_INGREDIENT_H
#define SQUAREPINE_OPENBREWER_INGREDIENT_H

class Ingredient
{
public:
    Ingredient (const Identifier& id) noexcept : state (id) { jassert (id.isValid()); }
    virtual ~Ingredient() noexcept { }

    Identifier getId() const noexcept { return state.getType(); }
    const ValueTree& getState() const noexcept { return state; }

protected:
    ValueTree state;

private:
    Ingredient() = delete;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Ingredient)
};

#endif //SQUAREPINE_OPENBREWER_INGREDIENT_H
