#ifndef SQUAREPINE_OPENBREWER_CARBONATION_H
#define SQUAREPINE_OPENBREWER_CARBONATION_H


/*
<Carbonation><_MOD_>2017-05-19</_MOD_>
<Name>Carbonation</Name>
<Type>7478</Type>
<Dirty>0</Dirty>
<Owndata>0</Owndata>
<TID>7478</TID>
<Size>9</Size>
<_XName>Carbonation</_XName>
<Allocinc>16</Allocinc>
<Data><Carbonation><_MOD_>2011-02-23</_MOD_>
<F_C_NAME>Corn Sugar</F_C_NAME>
<F_C_TEMPERATURE>70.0000000</F_C_TEMPERATURE>
<F_C_TYPE>0</F_C_TYPE>
<F_C_PRIMER_NAME>Corn Sugar</F_C_PRIMER_NAME>
<F_C_CARB_RATE>100.0000000</F_C_CARB_RATE>
<F_C_NOTES>Use corn sugar for priming the beer</F_C_NOTES>
</Carbonation>
*/

#endif //SQUAREPINE_OPENBREWER_CARBONATION_H
