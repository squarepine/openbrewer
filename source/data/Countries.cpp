struct Countries final
{
    Countries() noexcept { }

    void initialise()
    {
        if (! countries.empty())
            return;

        countries = std::initializer_list<Country>
        {
            {
                Country
                {
                    NEEDS_TRANS ("Canada"), "ca", "can", 124, Country::PostCodeType::postalCode, 
                    {
                        Country::Subdivision { NEEDS_TRANS ("Alberta"), "AB", Country::Subdivision::Type::province, 18 },
                        Country::Subdivision { NEEDS_TRANS ("British Columbia"), "BC", Country::Subdivision::Type::province, 19 },
                        Country::Subdivision { NEEDS_TRANS ("Manitoba"), "MB", Country::Subdivision::Type::province, 18 },
                        Country::Subdivision { NEEDS_TRANS ("New Brunswick"), "NB", Country::Subdivision::Type::province, 19 },
                        Country::Subdivision { NEEDS_TRANS ("Nunavut"), "NV", Country::Subdivision::Type::territory, 19 },
                        Country::Subdivision { NEEDS_TRANS ("Northwest Territories"), "NT", Country::Subdivision::Type::territory, 19 },
                        Country::Subdivision { NEEDS_TRANS ("Nova Scotia"), "NS", Country::Subdivision::Type::province, 19 },
                        Country::Subdivision { NEEDS_TRANS ("Newfoundland and Labrador"), "NL", Country::Subdivision::Type::province, 19 },
                        Country::Subdivision { NEEDS_TRANS ("Ontario"), "ON", Country::Subdivision::Type::province, 19 },
                        Country::Subdivision { NEEDS_TRANS ("Prince Edward Island"), "PE", Country::Subdivision::Type::province, 19 },
                        Country::Subdivision { NEEDS_TRANS ("Quebec"), "QC", Country::Subdivision::Type::province, 18 },
                        Country::Subdivision { NEEDS_TRANS ("Saskatechewan"), "SK", Country::Subdivision::Type::province, 19 },
                        Country::Subdivision { NEEDS_TRANS ("Yukon"), "YT", Country::Subdivision::Type::province, 19 }
                    }
                }
            },
            {
                Country
                {
                    NEEDS_TRANS ("United States of America"), "us", "usa", 840, Country::PostCodeType::zipCode, 
                    {
                        Country::Subdivision { NEEDS_TRANS ("Alabama"), "AL", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Alaska"), "AK", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Arizona"), "AZ", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Arkansas"), "AR", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("California"), "CA", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Colorado"), "CO", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Connecticut"), "CT", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Delaware"), "DE", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Florida"), "FL", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Georgia"), "GA", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Hawaii"), "HI", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Idaho"), "ID", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Illinois"), "IL", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Indiana"), "IN", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Iowa"), "IA", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Kansas"), "KS", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Kentucky"), "KY", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Louisiana"), "LA", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Maine"), "ME", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Maryland"), "MD", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Massachusetts"), "MA", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Michigan"), "MI", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Minnesota"), "MN", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Mississippi"), "MS", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Missouri"), "MO", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Montana"), "MT", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Nebraska"), "NE", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Nevada"), "NV", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("New Hampshire"), "NH", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("New Jersey"), "NJ", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("New Mexico"), "NM", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("New York"), "NY", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("North Carolina"), "NC", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("North Dakota"), "ND", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Ohio"), "OH", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Oklahoma"), "OK", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Oregon"), "OR", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Pennsylvania"), "PA", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Rhode Island"), "RI", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("South Carolina"), "SC", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("South Dakota"), "SD", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Tennessee"), "TN", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Texas"), "TX", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Utah"), "UT", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Vermont"), "VT", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Virginia"), "VA", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Washington"), "WA", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("West Virginia"), "WV", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Wisconsin"), "WI", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("Wyoming"), "WY", Country::Subdivision::Type::state, 21 },
                        Country::Subdivision { NEEDS_TRANS ("District of Columbia"), "DC", Country::Subdivision::Type::district, 21 }
                    }
                }
            },
            {
                Country
                {
                    NEEDS_TRANS ("Mexico"), "mx", "mex", 484, Country::PostCodeType::zipCode, 
                    {
                        Country::Subdivision { NEEDS_TRANS ("Aguascalientes"), "AGU", Country::Subdivision::Type::state, 18 },
                        Country::Subdivision { NEEDS_TRANS ("Baja California"), "BCN", Country::Subdivision::Type::state, 18 },
                        Country::Subdivision { NEEDS_TRANS ("Baja California Sur"), "BCS", Country::Subdivision::Type::state, 18 },
                        Country::Subdivision { NEEDS_TRANS ("Campeche"), "CAM", Country::Subdivision::Type::state, 18 },
                        Country::Subdivision { NEEDS_TRANS ("Ciudad de Mexico"), "CMX", Country::Subdivision::Type::district, 18 },
                        Country::Subdivision { NEEDS_TRANS ("Coahuila de Zaragoza"), "COA", Country::Subdivision::Type::state, 18 },
                        Country::Subdivision { NEEDS_TRANS ("Colima"), "COL", Country::Subdivision::Type::state, 18 },
                        Country::Subdivision { NEEDS_TRANS ("Chiapas"), "CHP", Country::Subdivision::Type::state, 18 },
                        Country::Subdivision { NEEDS_TRANS ("Chihuahua"), "CHH", Country::Subdivision::Type::state, 18 },
                        Country::Subdivision { NEEDS_TRANS ("Durango"), "DUR", Country::Subdivision::Type::state, 18 },
                        Country::Subdivision { NEEDS_TRANS ("Guanajuato"), "GUA", Country::Subdivision::Type::state, 18 },
                        Country::Subdivision { NEEDS_TRANS ("Guerrero"), "GRO", Country::Subdivision::Type::state, 18 },
                        Country::Subdivision { NEEDS_TRANS ("Hidalgo"), "HID", Country::Subdivision::Type::state, 18 },
                        Country::Subdivision { NEEDS_TRANS ("Jalisco"), "JAL", Country::Subdivision::Type::state, 18 },
                        Country::Subdivision { NEEDS_TRANS ("Mexico"), "MEX", Country::Subdivision::Type::state, 18 },
                        Country::Subdivision { NEEDS_TRANS ("Michoacan de Ocampo"), "MIC", Country::Subdivision::Type::state, 18 },
                        Country::Subdivision { NEEDS_TRANS ("Morelos"), "MOR", Country::Subdivision::Type::state, 18 },
                        Country::Subdivision { NEEDS_TRANS ("Nayarit"), "NAY", Country::Subdivision::Type::state, 18 },
                        Country::Subdivision { NEEDS_TRANS ("Nuevo Leon"), "NLE", Country::Subdivision::Type::state, 18 },
                        Country::Subdivision { NEEDS_TRANS ("Oaxaca"), "OAX", Country::Subdivision::Type::state, 18 },
                        Country::Subdivision { NEEDS_TRANS ("Puebla"), "PUE", Country::Subdivision::Type::state, 18 },
                        Country::Subdivision { NEEDS_TRANS ("Queretaro"), "QUE", Country::Subdivision::Type::state, 18 },
                        Country::Subdivision { NEEDS_TRANS ("Quintana Roo"), "ROO", Country::Subdivision::Type::state, 18 },
                        Country::Subdivision { NEEDS_TRANS ("San Luis Potosi"), "SLP", Country::Subdivision::Type::state, 18 },
                        Country::Subdivision { NEEDS_TRANS ("Sinaloa"), "SIN", Country::Subdivision::Type::state, 18 },
                        Country::Subdivision { NEEDS_TRANS ("Sonora"), "SON", Country::Subdivision::Type::state, 18 },
                        Country::Subdivision { NEEDS_TRANS ("Tabasco"), "TAB", Country::Subdivision::Type::state, 18 },
                        Country::Subdivision { NEEDS_TRANS ("Tamaulipas"), "TAM", Country::Subdivision::Type::state, 18 },
                        Country::Subdivision { NEEDS_TRANS ("Tlaxcala"), "TLA", Country::Subdivision::Type::state, 18 },
                        Country::Subdivision { NEEDS_TRANS ("Veracruz de Ignacio de la Llave"), "VER", Country::Subdivision::Type::state, 18 },
                        Country::Subdivision { NEEDS_TRANS ("Yucatan"), "YUC", Country::Subdivision::Type::state, 18 },
                        Country::Subdivision { NEEDS_TRANS ("Zacatecas"), "ZAC", Country::Subdivision::Type::state, 18 }
                    }
                }
            },
        };
    }

    std::vector<Country> countries;
};

static SharedResourcePointer<Countries> countriesResource;

const std::vector<Country>& getCountries()
{
    countriesResource->initialise();
    return countriesResource->countries;
}

//==============================================================================
struct Continents final
{
    Continents() noexcept { }

    void initialise()
    {
        if (! continents.empty())
            return;
    }

    std::vector<Continent> continents;
};

static SharedResourcePointer<Continents> continentsResource;

const std::vector<Continent>& getContinents()
{
    continentsResource->initialise();
    return continentsResource->continents;
}
