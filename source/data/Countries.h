#ifndef SQUAREPINE_OPENBREWER_COUNTRIES_H
#define SQUAREPINE_OPENBREWER_COUNTRIES_H

//==============================================================================
/** */
struct Language final
{
    /** */
    String getName() const noexcept { return TRANS (name); }

    String name, iso639_1, iso639_2;
};

//==============================================================================
/** */
struct Currency final
{
    /** */
    String getName() const noexcept { return TRANS (name); }

    String name, symbol;
};

//==============================================================================
/** A country, with its contents represented in the form of a UN member state. */
struct Country final
{
    /** */
    String getName() const noexcept { return TRANS (name); }

    //==============================================================================
    /** */
    struct Subdivision final
    {
        String getName() const noexcept { return TRANS (name); }

        /** */
        enum class Type
        {
            department,
            district,
            emirate,
            parish,
            province,
            rayon,
            region,
            state,
            territory
        };

        /** */
        String getTypeAsString() const noexcept
        {
            switch (type)
            {
                case Type::department:  return TRANS ("Department");
                case Type::district:    return TRANS ("District");
                case Type::emirate:     return TRANS ("Emirate");
                case Type::parish:      return TRANS ("Parish");
                case Type::province:    return TRANS ("Province");
                case Type::region:      return TRANS ("Region");
                case Type::rayon:       return TRANS ("Rayon");
                case Type::state:       return TRANS ("State");
                case Type::territory:   break;

                default: jassertfalse; break;
            };

            return TRANS ("Territory");
        }

        String name, iso3166_2;
        Type type = Subdivision::Type::territory;
        int legalDrinkingAge = 18;
    };

    //==============================================================================
    /** */
    enum class PostCodeType
    {
        postalCode,
        postcode,
        zipCode
    };

    /** */
    String getPostCodeTypeAsString() const noexcept
    {
        if (postCodeType == PostCodeType::postalCode)       return TRANS ("Postal Code");
        else if (postCodeType == PostCodeType::postcode)    return TRANS ("Postcode");

        return TRANS ("Zip Code");
    }

    //==============================================================================
    String name, iso3166_1_alpha2, iso3166_1_alpha3;
    int numericCode;
    PostCodeType postCodeType;
    std::vector<Subdivision> subdivisions;
};

//==============================================================================
/** */
struct Continent final
{
    /** */
    String getName() const noexcept { return TRANS (name); }

    String name;
    std::vector<int> numericCountryCodes;
};

//==============================================================================
/** */
const std::vector<Country>& getCountries();

/** */
const std::vector<Continent>& getContinents();

#endif //SQUAREPINE_OPENBREWER_COUNTRIES_H
