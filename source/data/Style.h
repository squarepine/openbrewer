#ifndef SQUAREPINE_OPENBREWER_STYLE_H
#define SQUAREPINE_OPENBREWER_STYLE_H

/*
<Style><_MOD_>2018-04-13</_MOD_>
<F_S_NAME>American Light Lager</F_S_NAME>
<F_S_CATEGORY>Standard American Beer</F_S_CATEGORY>
<F_S_GUIDE>BJCP 2015</F_S_GUIDE>
<F_S_LETTER>1</F_S_LETTER>
<F_S_NUMBER>1</F_S_NUMBER>
<F_S_WINE_COLOR>-1</F_S_WINE_COLOR>
<F_S_TYPE>1</F_S_TYPE>
<F_S_MIN_OG>1.0280000</F_S_MIN_OG>
<F_S_MAX_OG>1.0400000</F_S_MAX_OG>
<F_S_MIN_FG>0.9980000</F_S_MIN_FG>
<F_S_MAX_FG>1.0080000</F_S_MAX_FG>
<F_S_MIN_IBU>8.0000000</F_S_MIN_IBU>
<F_S_MAX_IBU>12.0000000</F_S_MAX_IBU>
<F_S_MIN_CARB>2.5000000</F_S_MIN_CARB>
<F_S_MAX_CARB>2.9000000</F_S_MAX_CARB>
<F_S_MIN_COLOR>2.0000000</F_S_MIN_COLOR>
<F_S_MAX_COLOR>3.0000000</F_S_MAX_COLOR>
<F_S_MIN_ABV>2.8000000</F_S_MIN_ABV>
<F_S_MAX_ABV>4.2000000</F_S_MAX_ABV>
<F_S_DESCRIPTION>Highly carbonated, very light-bodied, nearly flavorless lager designed to be consumed very cold. Very refreshing and thirst quenching. History: Coors briefly made a light lager in the early 1940s. Modern versions were first produced by Rheingold in 1967 to appeal to diet-conscious drinkers, but only became popular starting in 1973 after Miller Brewing acquired the recipe and marketed the beer heavily to sports fans with the &ldquo;tastes great, less filling&rdquo; campaign. Beers of this genre became the largest sellers in the United States in the 1990s. Style Comparison: A lighter-bodied, lower-alcohol, lower calorie version of an American Lager. Less hop character and bitterness than a Leichtbier. </F_S_DESCRIPTION>
<F_S_PROFILE>Appearance: Very pale straw to pale yellow color. White, frothy head seldom persists. Very clear. Flavor: Relatively neutral palate with a crisp and dry finish and a low to very low grainy or corn-like flavor that might be perceived as sweetness due to the low bitterness. Hop flavor ranges from none to low levels, and can have a floral, spicy, or herbal quality (although rarely strong enough to detect). Low to very low hop bitterness. Balance may vary from slightly malty to slightly bitter, but is relatively close to even. High levels of carbonation may accentuate the crispness of the dry finish. Clean lager fermentation character.
Mouthfeel: Very light (sometimes watery) body. Very highly carbonated with slight carbonic bite on the tongue. Comments: Designed to appeal to as broad a range of the general public as possible. Strong flavors are a fault.
</F_S_PROFILE>
<F_S_INGREDIENTS>Two- or six-row barley with high percentage (up to 40%) of rice or corn as adjuncts. Additional enzymes can further lighten the body and lower carbohydrates.</F_S_INGREDIENTS>
<F_S_EXAMPLES>Bud Light, Coors Light, Keystone Light, Michelob Light, Miller Lite, Old Milwaukee Light</F_S_EXAMPLES>
<F_S_WEB_LINK>http://www.bjcp.org</F_S_WEB_LINK>
</Style>
*/

#endif //SQUAREPINE_OPENBREWER_STYLE_H
