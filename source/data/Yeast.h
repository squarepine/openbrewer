#ifndef SQUAREPINE_OPENBREWER_YEAST_H
#define SQUAREPINE_OPENBREWER_YEAST_H

/*
<Yeast><_MOD_>2018-05-05</_MOD_>
<Name>Yeast</Name>
<Type>7426</Type>
<Dirty>1</Dirty>
<Owndata>0</Owndata>
<TID>100</TID>
<Size>463</Size>
<_XName>Yeast</_XName>
<Allocinc>16</Allocinc>
<Data><Yeast><_MOD_>2018-04-20</_MOD_>
<F_Y_NAME>Abbey Ale</F_Y_NAME>
<F_Y_LAB>White Labs</F_Y_LAB>
<F_Y_PRODUCT_ID>WLP530</F_Y_PRODUCT_ID>
<F_Y_TYPE>0</F_Y_TYPE>
<F_Y_FORM>0</F_Y_FORM>
<F_Y_FLOCCULATION>1</F_Y_FLOCCULATION>
<F_Y_STARTER_SIZE>1.2000000</F_Y_STARTER_SIZE>
<F_Y_AMOUNT>1.0000000</F_Y_AMOUNT>
<F_Y_INVENTORY>0.0000000</F_Y_INVENTORY>
<F_Y_TOLERANCE>13.0000000</F_Y_TOLERANCE>
<F_Y_PRICE>6.0000000</F_Y_PRICE>
<F_ORDER>0</F_ORDER>
<F_Y_IN_RECIPE>0</F_Y_IN_RECIPE>
<F_Y_BREW_DATE>2011-02-27</F_Y_BREW_DATE>
<F_Y_PKG_DATE>2011-02-27</F_Y_PKG_DATE>
<F_Y_CELLS>100.0000000</F_Y_CELLS>
<F_Y_MIN_ATTENUATION>75.0000000</F_Y_MIN_ATTENUATION>
<F_Y_MAX_ATTENUATION>80.0000000</F_Y_MAX_ATTENUATION>
<F_Y_MIN_TEMP>66.0000000</F_Y_MIN_TEMP>
<F_Y_MAX_TEMP>72.0000000</F_Y_MAX_TEMP>
<F_Y_USE_STARTER>0</F_Y_USE_STARTER>
<F_Y_ADD_TO_SECONDARY>0</F_Y_ADD_TO_SECONDARY>
<F_Y_TIMES_CULTURED>0</F_Y_TIMES_CULTURED>
<F_Y_MAX_REUSE>5</F_Y_MAX_REUSE>
<F_Y_CULTURE_DATE>2003-06-14</F_Y_CULTURE_DATE>
<F_Y_BEST_FOR>Belgian Trappist Ale, Spiced Ale, Trippel, Dubbel, Grand Cru</F_Y_BEST_FOR>
<F_Y_NOTES>Used in two of six remaining Trappist breweries.  Distinctive plum and fruitiness.  Good for high gravity beers.</F_Y_NOTES>
<F_Y_AGE_RATE>10.0000000</F_Y_AGE_RATE>
</Yeast>
*/

#endif //SQUAREPINE_OPENBREWER_YEAST_H
