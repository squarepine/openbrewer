#ifndef SQUAREPINE_OPENBREWER_HOPS_H
#define SQUAREPINE_OPENBREWER_HOPS_H

/*
<Hops>
<_MOD_>2018-04-17</_MOD_>
<Name>Hops</Name>
<Type>7403</Type>
<Dirty>0</Dirty>
<Owndata>0</Owndata>
<TID>7403</TID>
<Size>266</Size>
<_XName>Hops</_XName>
<Allocinc>16</Allocinc>
<Data><Hops><_MOD_>2003-06-09</_MOD_>
<F_H_NAME>Admiral</F_H_NAME>
<F_H_ORIGIN>United Kingdom</F_H_ORIGIN>
<F_H_TYPE>0</F_H_TYPE>
<F_H_FORM>0</F_H_FORM>
<F_H_ALPHA>14.7500000</F_H_ALPHA>
<F_H_BETA>5.6000000</F_H_BETA>
<F_H_PERCENT>0.0000000</F_H_PERCENT>
<F_H_INVENTORY>0.0000000</F_H_INVENTORY>
<F_H_AMOUNT>0.0000000</F_H_AMOUNT>
<F_H_HSI>15.0000000</F_H_HSI>
<F_H_BOIL_TIME>0.0000000</F_H_BOIL_TIME>
<F_H_DRY_HOP_TIME>3.0000000</F_H_DRY_HOP_TIME>
<F_H_NOTES>Bittering hops derived from Wye Challenger.  Good high-alpha bittering hops.
Used for: Ales
Aroma: Primarily for bittering
Substitutes: Target, Northdown, Challenger
</F_H_NOTES>
<F_H_WHIRLPOOL_TEMP>194.4406400</F_H_WHIRLPOOL_TEMP>
<F_H_IBU_CONTRIB>0.0000000</F_H_IBU_CONTRIB>
<F_ORDER>0</F_ORDER>
<F_H_USE>0</F_H_USE>
<F_H_IN_RECIPE>0</F_H_IN_RECIPE>
<F_H_PRICE>1.0000000</F_H_PRICE>
</Hops>

*/

class Hops final : public Ingredient
{
public:
    Hops() noexcept : Ingredient ("Hops") { }
    virtual ~Hops() noexcept { }

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Hops)
};

#endif //SQUAREPINE_OPENBREWER_HOPS_H
