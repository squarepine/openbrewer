#ifndef SQUAREPINE_OPENBREWER_LOOK_AND_FEEL_H
#define SQUAREPINE_OPENBREWER_LOOK_AND_FEEL_H

#include "JuceHeader.h"

//==============================================================================
namespace DefaultSizes
{
    constexpr auto toolbarAndTabBarSize = 50;
    constexpr auto corner = float_Pi;
    constexpr auto margin = 10;
    constexpr auto gridMargin = 10;
    constexpr auto menuBarHeight = 24;
}

//==============================================================================
struct SharedFonts final
{
    SharedFonts() {}
    ~SharedFonts() {}

    struct TypefaceFamily final
    {
        struct Weight final
        {
            Typeface::Ptr thin, ultraLight, light, semiLight,
                          normal, medium, semiBold, bold, extraBold,
                          black, extraBlack, ultraBlack;
        };

        Weight regular, italic;
    };

    /** Loads up the typeface and font instances.

        This will only load up things once by checking
        if the objects are valid or not.
    */
    void initialise();

    static Typeface::Ptr getTypefaceFromFamily (const Font& source, const String& nameToLookFor,
                                                Typeface::Ptr regular,
                                                Typeface::Ptr bold = nullptr,
                                                Typeface::Ptr italic = nullptr,
                                                Typeface::Ptr boldItalic = nullptr);

    static Typeface::Ptr getTypefaceFromFamily (const Font& source, const String& nameToLookFor,
                                                const TypefaceFamily& family);

    TypefaceFamily plex;
};

//==============================================================================
struct SharedResources final
{
    SharedResources() {}
    ~SharedResources() {}

    void initialise();

    SharedFonts sharedFonts;
};

//==============================================================================
class OpenBrewerLookAndFeel final : public LookAndFeel_V4
{
public:
    OpenBrewerLookAndFeel();
    ~OpenBrewerLookAndFeel();

    static void drawTextInRegion (Graphics& g, const Font& f, const String& text, Justification j,
                                  const Rectangle<int>& region, Colour colour = Colours::darkgrey, bool isEnabled = true);

    static Grid createDefaultGrid();

    //==============================================================================
    int getMenuWindowFlags() override { return 0; }
    int getAlertBoxWindowFlags() override { return ComponentPeer::windowAppearsOnTaskbar; }
    void playAlertSound() override {}
    Typeface::Ptr getTypefaceForFont (const Font&) override;
    void drawTooltip (Graphics&, const String&, int, int) override;
    void drawLabel (Graphics&, Label&) override;
    Font getLabelFont (Label&) override;
    BorderSize<int> getLabelBorderSize (Label& label) override { return label.getBorderSize(); }
    int getTabButtonSpaceAroundImage() override;
    int getTabButtonOverlap (int) override;
    int getTabButtonBestWidth (TabBarButton&, int) override;
    Rectangle<int> getTabButtonExtraComponentBounds (const TabBarButton&, Rectangle<int>&, Component&) override;
    void drawTabButton (TabBarButton&, Graphics&, bool, bool) override;
    Font getTabButtonFont (TabBarButton&, float) override;
    void drawTabButtonText (TabBarButton&, Graphics&, bool, bool) override;
    void drawTabbedButtonBarBackground (TabbedButtonBar&, Graphics&) override;
    void drawTabAreaBehindFrontButton (TabbedButtonBar&, Graphics&, int, int) override;
    void createTabButtonShape (TabBarButton&, Path&, bool, bool) override;
    void fillTabButtonShape (TabBarButton&, Graphics&, const Path&, bool, bool) override;
    Button* createTabBarExtrasButton() override;
    void drawComboBox (Graphics&, int, int, bool, int, int, int, int, ComboBox&) override;
    void drawButtonBackground (Graphics&, Button&, const Colour&, bool, bool) override;
    Font getTextButtonFont (TextButton&, int) override;
    int getTextButtonWidthToFitText (TextButton&, int) override;
    void drawButtonText (Graphics&, TextButton&, bool, bool) override;
    void drawToggleButton (Graphics&, ToggleButton&,  bool, bool) override;
    void changeToggleButtonWidthToFitText (ToggleButton&) override;
    void drawTickBox (Graphics&, Component&, float, float, float, float, bool, bool, bool, bool) override;
    void drawDrawableButton (Graphics&, DrawableButton&, bool, bool) override;
    void fillTextEditorBackground (Graphics&, int, int, TextEditor&) override;
    void drawTextEditorOutline (Graphics&, int, int, TextEditor&) override;
    float getCallOutBoxCornerSize (const CallOutBox&) override;
    Font getMenuBarFont (MenuBarComponent&, int, const String&) override;
    void drawPopupMenuItem (Graphics&, const Rectangle<int>&, bool, bool, bool, bool, bool, const String&, const String&, const Drawable*, const Colour*) override;

    SharedResourcePointer<SharedResources> sharedResources;

private:
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (OpenBrewerLookAndFeel)
};

#endif //SQUAREPINE_OPENBREWER_LOOK_AND_FEEL_H
