#include "MainComponent.h"

MainComponent::MainComponent()
{
    menuBar.setModel (this);
    addAndMakeVisible (menuBar);

    commandManager.registerAllCommandsForTarget (this);
    setApplicationCommandManagerToWatch (&commandManager);
    setSize (1024, 768);

   #if JUCE_DEBUG
    for (const auto& c : getCountries())
    {
        DBG (c.getName());

        for (const auto& sd : c.subdivisions)
            DBG ("\t- " + sd.getName());
    }
   #endif
}

MainComponent::~MainComponent()
{
    menuBar.setModel (nullptr);
    setApplicationCommandManagerToWatch (nullptr);
}

//==============================================================================
StringArray MainComponent::getMenuBarNames()
{
    return 
    {
        TRANS ("File"),
        TRANS ("Edit"),
        TRANS ("View"),
        TRANS ("Tools"),
        TRANS ("Help")
    };
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String&)
{
    PopupMenu menu;

    if (topLevelMenuIndex == 0)
    {
        menu.addCommandItem (&commandManager, StandardApplicationCommandIDs::quit);
    }

    return menu;
}

void MainComponent::menuItemSelected (int, int)
{
}

//==============================================================================
ApplicationCommandTarget* MainComponent::getNextCommandTarget()
{
    return nullptr;
}

void MainComponent::getAllCommands (Array<CommandID>& commands)
{
    commands.add (StandardApplicationCommandIDs::quit);
}

void MainComponent::getCommandInfo (const CommandID commandID, ApplicationCommandInfo& result)
{
    if (commandID == StandardApplicationCommandIDs::quit)
    {
        result.setInfo (TRANS ("Exit"),
                        TRANS ("Exits the application."),
                        "Application", 0);

       #if JUCE_WINDOWS
        result.defaultKeypresses.add (KeyPress::createFromDescription ("ALT+F4"));
       #elif JUCE_MAC || JUCE_LINUX
        result.defaultKeypresses.add (KeyPress ('q', ModifierKeys::commandModifier, 0));
       #endif
    }
}

bool MainComponent::perform (const InvocationInfo& info)
{
    if (info.commandID == StandardApplicationCommandIDs::quit)
    {
        JUCEApplication::getInstance()->systemRequestedQuit();
        return true;
    }

    return false;
}

//==============================================================================
void MainComponent::resized()
{
    auto b = getLocalBounds();

    menuBar.setBounds (b.removeFromTop (DefaultSizes::menuBarHeight));
}
