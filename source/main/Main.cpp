#include "MainComponent.h"

class OpenBrewerApplication final : public JUCEApplication
{
public:
    OpenBrewerApplication() {}

    const String getApplicationName() override              { return ProjectInfo::projectName; }
    const String getApplicationVersion() override           { return ProjectInfo::versionString; }
    bool moreThanOneInstanceAllowed() override              { return false; }
    void initialise (const String&) override                { mainWindow.reset (new MainWindow()); }
    void shutdown() override                                { mainWindow = nullptr; }
    void systemRequestedQuit() override                     { quit(); }
    void anotherInstanceStarted (const String&) override    { }

private:
    class MainWindow final : public DocumentWindow
    {
    public:
        MainWindow() :
            DocumentWindow ({}, Colours::grey, DocumentWindow::allButtons)
        {
            Desktop::getInstance().setDefaultLookAndFeel (globalLookAndFeel);

            setOpaque (true);
            setUsingNativeTitleBar (true);
            setContentOwned (new MainComponent(), true);

            {
                String title;
                title << ProjectInfo::projectName
                      << " v" << ProjectInfo::versionString
                      << " - " << ProjectInfo::companyName;

               #if JUCE_DEBUG
                title << " [" << "DEBUG" << "]";
               #endif
                setName (title);
            }

            {
                constexpr auto imageSize = 48;
                Image iconImage (Image::PixelFormat::ARGB, imageSize, imageSize, true);

                {
                    Graphics g (iconImage);
                    auto d = Drawable::createFromImageData (BinaryData::beer_svg, BinaryData::beer_svgSize);
                    jassert (d != nullptr);
                    d->drawWithin (g, Rectangle<int> (imageSize, imageSize).toFloat(), RectanglePlacement::centred, 1.0f);
                }

                trayIcon.setIconImage (iconImage, iconImage);
            }

           #if JUCE_IOS || JUCE_ANDROID
            setFullScreen (true);
           #else
            setResizable (true, false);
            setResizeLimits (640, 360, std::numeric_limits<int>::max(), std::numeric_limits<int>::max());
            centreWithSize (getWidth(), getHeight());
           #endif

            setVisible (true);
        }

        ~MainWindow()
        {
            //Only kill off the L&F once we reach a point where this is the only processor instance:
            if (globalLookAndFeel.getReferenceCount() <= 1)
                Desktop::getInstance().setDefaultLookAndFeel (nullptr);
        }

        void closeButtonPressed() override
        {
            JUCEApplication::getInstance()->systemRequestedQuit();
        }

    private:
        SharedResourcePointer<OpenBrewerLookAndFeel> globalLookAndFeel;
        SystemTrayIconComponent trayIcon;

        JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainWindow)
    };

    std::unique_ptr<MainWindow> mainWindow;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (OpenBrewerApplication)
};

//==============================================================================
START_JUCE_APPLICATION (OpenBrewerApplication)
