#ifndef SQUAREPINE_OPENBREWER_MAIN_COMPONENT_H
#define SQUAREPINE_OPENBREWER_MAIN_COMPONENT_H

#include "../data/Data.h"
#include "../graphics/OpenBrewerLookAndFeel.h"

class MainComponent final : public Component,
                            public MenuBarModel,
                            public ApplicationCommandTarget
{
public:
    MainComponent();
    ~MainComponent();

    //==============================================================================
    /** @internal */
    void resized() override;
    /** @internal */
    StringArray getMenuBarNames() override;
    /** @internal */
    PopupMenu getMenuForIndex (int topLevelMenuIndex, const String& menuName) override;
    /** @internal */
    void menuItemSelected (int menuItemID, int topLevelMenuIndex) override;
    /** @internal */
    ApplicationCommandTarget* getNextCommandTarget() override;
    /** @internal */
    void getCommandInfo (CommandID, ApplicationCommandInfo&) override;
    /** @internal */
    void getAllCommands (Array<CommandID>&) override;
    /** @internal */
    bool perform (const InvocationInfo&) override;

private:
    //==============================================================================
    ApplicationCommandManager commandManager;

    MenuBarComponent menuBar;

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};

#endif //SQUAREPINE_OPENBREWER_MAIN_COMPONENT_H
